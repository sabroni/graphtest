﻿angular.module("testGraph", [])
    // import the test data as a constant
.constant("data", [
  {
      "entity_type": "country",
      "geo_json": {
          "coordinates": [
            "-64.020545",
            "-34.845871"
          ],
          "type": "Point"
      },
      "name": "Argentina",
      "scores": {
          "com_certifiedsustainableproduction_2018-Q1": {
              "value": 2.66666666667
          },
          "com_childlabour_2018-Q1": {
              "value": 5.16666666667
          },
          "com_climatechangevulnerability_2018-Q1": {
              "value": 9.33333333333
          },
          "com_collectivebargaining_2018-Q1": {
              "value": 7.33333333333
          },
          "com_corruption_2018-Q1": {
              "value": 3.66666666667
          },
          "com_deforestation_2018-Q1": {
              "value": 5
          },
          "com_discrimination_2018-Q1": {
              "value": 1.33333333333
          },
          "com_forcedlabour_2018-Q1": {
              "value": 0.55555555556
          }
      }
  },
  {
      "entity_type": "country",
      "geo_json": {
          "coordinates": [
            "24.334412",
            "-29.311549"
          ],
          "type": "Point"
      },
      "name": "South Africa",
      "scores": {
          "com_certifiedsustainableproduction_2018-Q1": {
              "value": 1.33333333333
          },
          "com_childlabour_2018-Q1": {
              "value": 1.5
          },
          "com_climatechangevulnerability_2018-Q1": {
              "value": 2
          },
          "com_collectivebargaining_2018-Q1": {
              "value": 1.66666666667
          },
          "com_corruption_2018-Q1": {
              "value": 1.33333333333
          },
          "com_deforestation_2018-Q1": {
              "value": 1.66666666667
          },
          "com_discrimination_2018-Q1": {
              "value": 1.66666666667
          },
          "com_forcedlabour_2018-Q1": {
              "value": 1.88888888889
          }
      }
  },
  {
      "entity_type": "country",
      "geo_json": {
          "coordinates": [
            "3.334412",
            "55.311549"
          ],
          "type": "Point"
      },
      "name": "United Kingdom",
      "scores": {
          "com_certifiedsustainableproduction_2018-Q1": {
              "value": 6.666666
          },
          "com_childlabour_2018-Q1": {
              "value": 9.5
          },
          "com_climatechangevulnerability_2018-Q1": {
              "value": 6.666666
          },
          "com_collectivebargaining_2018-Q1": {
              "value": 8.199999
          },
          "com_corruption_2018-Q1": {
              "value": 7.87162
          },
          "com_deforestation_2018-Q1": {
              "value": 6.66666
          },
          "com_discrimination_2018-Q1": {
              "value": 5.55555
          },
          "com_forcedlabour_2018-Q1": {
              "value": 5.0
          }
      }
  },
  {
      "entity_type": "country",
      "geo_json": {
          "coordinates": [
            "71.4412",
            "-35.6751"
          ],
          "type": "Point"
      },
      "name": "Chile",
      "scores": {
          "com_certifiedsustainableproduction_2018-Q1": {
              "value": 0
          },
          "com_childlabour_2018-Q1": {
              "value": 8.5
          },
          "com_climatechangevulnerability_2018-Q1": {
              "value": 4
          },
          "com_collectivebargaining_2018-Q1": {
              "value": 8.66666666667
          },
          "com_corruption_2018-Q1": {
              "value": 5.33333333333
          },
          "com_deforestation_2018-Q1": {
              "value": 4.66666666667
          },
          "com_discrimination_2018-Q1": {
              "value": 7.66666666667
          },
          "com_forcedlabour_2018-Q1": {
              "value": 1.88888888889
          }
      }
  },
    {
        "entity_type": "country",
        "geo_json": {
            "coordinates": [
              "31.307",
              "6.8777"
            ],
            "type": "Point"
        },
        "name": "South Sudan",
        "scores": {
            "com_certifiedsustainableproduction_2018-Q1": {
                "value": 9.10000
            },
            "com_childlabour_2018-Q1": {
                "value": 0.2222222
            },
            "com_climatechangevulnerability_2018-Q1": {
                "value": 5
            },
            "com_collectivebargaining_2018-Q1": {
                "value": 7
            },
            "com_corruption_2018-Q1": {
                "value": 2.44444
            },
            "com_deforestation_2018-Q1": {
                "value": 1.9999
            },
            "com_discrimination_2018-Q1": {
                "value": 7.2222222
            },
            "com_forcedlabour_2018-Q1": {
                "value": 5.0000
            }
        }
    },
    {
        "entity_type": "country",
        "geo_json": {
            "coordinates": [
              "-2.2137",
              "46.2276"
            ],
            "type": "Point"
        },
        "name": "France",
        "scores": {
            "com_certifiedsustainableproduction_2018-Q1": {
                "value": 2.0
            },
            "com_childlabour_2018-Q1": {
                "value": 7.5
            },
            "com_climatechangevulnerability_2018-Q1": {
                "value": 8
            },
            "com_collectivebargaining_2018-Q1": {
                "value": 9.66666666667
            },
            "com_corruption_2018-Q1": {
                "value": 4.444444
            },
            "com_deforestation_2018-Q1": {
                "value": 3.6666666
            },
            "com_discrimination_2018-Q1": {
                "value": 8.66666666667
            },
            "com_forcedlabour_2018-Q1": {
                "value": 3.00000
            }
        }
    }

])
// define constants for list of countries and categories and give the categories nicer names
.constant("categories", {
    "Certified Sustainable Production": "com_certifiedsustainableproduction_2018-Q1",
    "Child Labour": "com_childlabour_2018-Q1",
    "Climate Change Vulnerability": "com_climatechangevulnerability_2018-Q1",
    "Collective Bargaining": "com_collectivebargaining_2018-Q1",
    "Corruption": "com_corruption_2018-Q1",
    "Deforestation": "com_deforestation_2018-Q1",
    "Discrimination": "com_discrimination_2018-Q1",
    "Forced Labour": "com_forcedlabour_2018-Q1"
}
)
.constant("countries", ["Argentina", "South Africa", "United Kingdom", "Chile", "South Sudan", "France"])
.factory("dataService", function dataServiceConstructor(data, categories, countries) {

    return {
        countryCategoryValue: function (country, category) {
            if (countries.indexOf(country) < 0 || !categories[category]) { return 0; }
            for (var i = 0; i < data.length; i++) {
                if (data[i].name === country) {
                    return 10 - data[i].scores[categories[category]].value;
                }
            }
            return 0;
        }
    };
})
    // test graph service, set it's properties and it exposes a collection of relevant columns
.factory("testGraphService", function testGraphServiceConstructor(dataService, categories, countries) {

    var filter = { category: "", country1: "", country2: "", graphType: "" };
    var columns = [];

    return {

        columns: columns,

        setCategory: function (category) {
            if (!angular.isString(category)) { return; }
            filter.category = category;
            if (filter.graphType === "Countries") {
                updateCategory();
            }
        },

        setCountry1: function (country1) {
            if (!angular.isString(country1)) { return; }
            if (countries.indexOf(country1) > -1 && filter.country2 !== country1) {
                filter.country1 = country1;
            }
            if (filter.graphType === "Categories") {
                updateCountry1();
            }
        },

        setCountry2: function (country2) {
            if (!angular.isString(country2)) { return; }
            if (countries.indexOf(country2) > -1 && filter.country1 !== country2) {
                filter.country2 = country2;
            }
            if (filter.graphType === "Categories") {
                updateCountry2();
            }
        },

        setGraphType: function (graphType) {
            if (!angular.isString(graphType)) { return; }
            if (graphType === "Countries" && graphType !== filter.graphType) {
                filter.graphType = graphType;
                createCountryColumns();
            } else if (graphType === "Categories" && filter.graphType !== graphType) {
                filter.graphType = "Categories";
                createCategoryColumns();
            }
        },

    };

    function createCountryColumns() {
        // don't destroy the columns collection, just blat it and reload
        columns.length = 0;
        angular.forEach(countries, function createCountryColumn(country, key) {
            columns.push(column(filter.category, country, country));
        });
    }

    function createCategoryColumns() {
        columns.length = 0;
        angular.forEach(categories, function createCategoryColumnPair(category, name) {
            columns.push(column(name, filter.country1, name));
            columns.push(column(name, filter.country2, name));
        });
    }

    function updateCategory() {
        angular.forEach(columns, function updateColumnCategory(column) {
            column.category = filter.category;
        });
    }

    function updateCountry1() {
        angular.forEach(columns, function updateColumnCountry1(column, count) {
            if (!(count & 1)) {
                column.country = filter.country1;
            }
        });
    }

    function updateCountry2() {
        angular.forEach(columns, function updateColumnCountry1(column, count) {
            if ((count & 1)) {
                column.country = filter.country2;
            }
        });
    }

    function column(category, country, text) {
        var countryClass;
        return {
            category: category,
            country: country,
            height: function calculateHeight() { return dataService.countryCategoryValue(this.country, this.category) * 10; },
            text: text,
            countryClass: function () {
                return this.country && this.country.replace(/\s+/g, '');
            }
        }
    };

})
.controller("testGraphCtrl", function testGraphCtrlFactory($scope, testGraphService, categories, countries) {

    $scope.graphTypes = ["Countries", "Categories"];
    $scope.categories = categories;
    $scope.countries1 = angular.copy(countries);
    $scope.countries2 = [];
    $scope.columns = testGraphService.columns;
    $scope.graphTypeSelected = function () {
        if ($scope.selectedGraphType) {
            testGraphService.setGraphType($scope.selectedGraphType);
        }
    };
    $scope.categorySelected = function () {
        if ($scope.selectedCategory) {
            testGraphService.setCategory($scope.selectedCategory);
        }
    };
    $scope.country1Selected = function () {
        if ($scope.selectedCountry1 && $scope.selectedCountry1 !== $scope.selectedCountry2) {
            testGraphService.setCountry1($scope.selectedCountry1);
            $scope.countries2 = filterCountries($scope.selectedCountry1);
        }
    };
    $scope.country2Selected = function () {
        if ($scope.selectedCountry2) {
            testGraphService.setCountry2($scope.selectedCountry2);
            $scope.countries1 = filterCountries($scope.selectedCountry2);
        }
    };
    function filterCountries(country){
        var countriesCopy = angular.copy(countries);
        var index = countriesCopy.indexOf(country);
        if (index > -1){
            countriesCopy.splice(index, 1);
            return countriesCopy;
        }
        return [];
    }
})